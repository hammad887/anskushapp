import React, { Component } from 'react';
import {StyleSheet,ScrollView, View ,Text,Image,TouchableOpacity,TouchableHighlight,TextInput,Button} from 'react-native';
import { Rating} from 'react-native-ratings'; 
import Fontisto from 'react-native-vector-icons/Fontisto';

export default class CustomerDashboard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [
       {id : "1" , category: "T-Shirt", title : "Men's Short Sleeve T-Shirt", Description: "Item part number: L24GMWBS-$P ASIN: B07NQHQ5BD Date first available at Amazon.in: 13 February 2019 Average Customer Review: 3.6 out of 5 stars 75", Price :6.7 , rating : "3.5" , favourite : false, image :"https://ae01.alicdn.com/kf/HTB1ei_Ov9BYBeNjy0Feq6znmFXae/Men-s-Tops-Tees-2019-summer-new-cotton-v-neck-short-sleeve-t-shirt-men-fashion.jpg", },
       {id : "2" , category: "T-Shirt", title : "Summer New V Neck Shirt", Description: "US $6.2 31% OFF|Men's Tops Tees 2019 summer new cotton v neck short sleeve t shirt men fashion trends fitnes…", Price :9.5 , rating : "3.4" , favourite : false, image :"https://5.imimg.com/data5/WL/IO/MY-3507052/stylish-v-neck-men-t-shirt-500x500.jpg", },
       {id : "3" , category: "T-Shirt", title : "Short Sleeve Men's T-Shirt", Description: "UGDXL Men Mandarin Collar T-Shirt Basic Tshirt Male Short Sleeve Shirt Tops Tees Cotton T Shirt | Amazon.com", Price :8.5 , rating : "4.3" , favourite : false, image :"https://images-na.ssl-images-amazon.com/images/I/61xHYLxJY%2BL._UL1500_.jpg", },
       {id : "4" , category: "T-Shirt", title : "Korean Fashion Men's T-Shirt", Description: "Blueberry - offering XXL Party Wear Korean Fashion Men's Pattern T-shirt at Rs 400/piece in Bhiwandi, Maharashtra. Get best price and read about company and get contact details and address....", Price :4.5 , rating : "4.1" , favourite : false, image :"https://5.imimg.com/data5/LY/AR/MY-40507433/korean-fashion-men-27s-pattern-t-shirt-500x500.jpg"},
       {id : "5" , category: "T-Shirt", title : "Nouveau Style Men's T-Shirt", Description: "Offre spéciale 2017 nouveau Style grande taille décontracté hommes T shirt mode T shirts d'été porter à manches long", Price :13.45 , rating : "4.6" , favourite : false, image :"https://www.dhresource.com/0x0s/f2-albu-g5-M01-CE-E7-rBVaI1mGU6yAFVISAAYboZFmkUI237.jpg/fashion-mens-slim-fit-long-sleeve-t-shirts.jpg", },
      ]
    };

  }

  render()
  {
    return (
      <View style={styles.container}>
         <ScrollView >
          <View style={{flexDirection: 'row'}}>
            <View style={styles.SectionStyle}>
                                  
           <Image source={require('./images/search.png')} style={styles.ImageStyle} tintColor='grey'/>

            <TextInput 
              placeholder = "Search"
              autoCapitalize = "none"
              placeholderTextColor="#666"
            >  
           </TextInput>
                                      
          </View>
          <TouchableOpacity onPress={() => navigate('Cart')}>
          <Image source={require('./images/shopping-cart.png')} style={{ marginTop: 15,marginBottom:15,marginLeft: 5, height:40,width: 40}} tintColor='grey'  />
          </TouchableOpacity>
          </View>
          {
           this.state.data.map(item =>(   
            <View key={item.id} style={styles.productMain}>
        <View style={{width:"35%", height:200, }}>
        <Image style={{width : "100%" , height:"95%" , resizeMode:"contain", borderRadius:5}} 
            source={{uri : item.image}} />
        </View>
        <View style={{ justifyContent: "space-around", alignContent:"center",  marginLeft:20,}}>
        <View style={{overFlow:"hidden"}}>
           <Text numberOfLines={1} style={styles.text}>{item.title}</Text>
             </View>
            <Text style={{color:"#666666"}}>Category : {item.category}</Text>
            <Text style={styles.text}>Price : ${item.Price}</Text>
            <Rating
                startingValue={ Math.floor(parseInt(item.rating))}
                    ratingCount={5}
                    imageSize={25} 
                    style={{alignItems:"flex-start"}}
            
                />
             <View style={{flexDirection:"row"}}>
             <TouchableOpacity style={{ 
                justifyContent:"center", 
                alignItems:"center", 
                padding:10 ,
                width:135, 
                backgroundColor:"white",
                borderColor: '#883cad',
                borderRadius:3,
                borderWidth:1,
                }} 
                >
              <Text style={{color:"#883cad", fontWeight:"bold"}}>Add to Cart</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                <Fontisto name="heart" size={33} color="#FF543C" style={{marginLeft:10}} />
                </TouchableOpacity>
              </View>
          </View>
          </View>
            ))
          }


        </ScrollView>
      </View>
    );
  }          
}

const styles= StyleSheet.create(
  {
    container:{
      backgroundColor: '#fff',
      flex:1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    checkoutcart:{
      flexDirection:'row',
      alignSelf:'center'
    },
    SectionStyle: {
      flexDirection: 'row',
      width:290,
      height: 50,
      padding: 10,
      marginVertical: 10,
      fontSize: 16,
      borderWidth: 0.5,
      borderRadius: 10,
      borderColor: '#C0C0C0',
      backgroundColor: 'rgba(0,0,0,0)', 
  },
   
    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 20,
        width: 20,
        resizeMode : 'stretch',
        alignItems: 'center',
         backgroundColor: 'rgba(0,0,0,0)',
    },
    productMain: {
      flexDirection:"row", 
      justifyContent:"flex-start", 
      borderBottomColor:"gray",
      borderBottomWidth:1,
      marginBottom:5
    
        
    },
});